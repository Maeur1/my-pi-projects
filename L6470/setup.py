from setuptools import setup

setup(  name='l6470',
        version='0.1',
        description='A Library for using the L6470 with raspberry pi'
        url=''
        author='Mayur Panchal'
        license='GPL'
        packages=['l6470']
        zip_safe=False)